
$(document).ready(function(){
    redir();
    mobMenu();
    $(".modal-wrapper").hide();
})

$(window).scroll(function(){
    header();
})

function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}
function header(){
    if($(document).scrollTop() > 100){
        $(".logo").css({
            "width" : "50%",
            "transition" : "0.5s"
        });
        $(".web-a").css({
            "font-size" : "8px",
            "transition" : "0.5s"
        })
    }
    else {
        $(".logo").css({
            "width" : "65%",
            "transition" : "0.5s"
        });
        $(".web-a").css({
            "font-size" : "11px",
            "transition" : "0.5s"
        })
    }
}

function redir(e){
    if(e == 'new_arrival'){
        window.location="./pages/new-arrivals.html";
    }
    if(e == 'careers'){
        window.location="./pages/careers.html";
    }
    if(e == 'news_letter'){
        window.location="./pages/contact-us.html";
    }
    if(e == 'reseller'){
        window.location="./pages/reseller.html";
    }
    if(e == 'promos'){
        window.location="./pages/promos.html";
    }
}

function mobMenu(e) {
    if(e == 1){
        console.log('ee')
        $(".mob-menu").css({
            'top'   :   '0'
        })
    }
    if(e == 2){
        console.log('ee')
        $(".mob-menu").css({
            'top'   :   '-100vh'
        })
    }
}

function imgModal(e){
    switch(e){
        case 1:
            var srcs = $('.img_1').attr('src')
            break;
        case 2:
            var srcs = $('.img_2').attr('src')
            break;
        case 3:
            var srcs = $('.img_3').attr('src')
            break;
        default:
            location.reload();
            break;
        }
    $(".pop-up-img").attr("src", srcs);
    $('.pop-up-img').fadeIn(100);
    $('.pop-holder').fadeIn(100);
    $('.pop-holder').on('click', function(){
        $('.pop-up-img').fadeOut(100);
        $('.pop-holder').fadeOut(100);
    })
}

$(document).ready(function () {

    let mconfirm = $('.email-confirm');
    mconfirm.hide();
    $(".email-main").keyup(function () {
    var value = $(this).val();
        if (value != ""){
            $(mconfirm).show();
        }
        else $(mconfirm).hide();
    });

    var form =  $("#NewsletterForm");
    var x = $(".email-main");
    var y = $(".email-confirm");

    $("#NewsletterForm").submit(function(event){
            var recaptcha = $("#g-recaptcha-response").val();
                if (recaptcha === "") {
                    event.preventDefault();
                    $("#form-subscribe-error").text("Kindly check the captcha before submitting again.");
                       
            }

            event.preventDefault();
            if(x.val() === y.val()){
            
                $('#form-subscribe').val('sending .. ');
                if(form.is("#NewsletterForm")){
                    event.preventDefault();
                    $.ajax({
                        type: form.attr("method"),
                        url: form.attr("action"),
                        data: form.serialize(),
                        success: function (data) {
                            $('#form-subscribe').val('subscribe')
                            $('.email').val('');
                            $("#form-subscribe-error").text("");
                            $("#form-subscribe-success").text("Thank you for subscribing!");
                            $(mconfirm).hide();
                            setTimeout(function(){
                                $("#form-subscribe-success").hide()
                            }, 2500)


                        },
                        error: function(data){
                            alert('Currently working locally. Disabled on live to prevent spams while in development')
                            $('#form-subscribe').val('subscribe');
                            $('.email').val('');
                            $('#email-confirm').val('');
                            $('#form-subscribe-error, #form-subscribe-success').text('');
                        }
                    });
                }

            }

            else {
                
                $("#form-subscribe-error").text("Email and confirmation email does not match. Please recheck your inputs before submitting again.");

            }

        })
});

function vidModal(e) {
    if(e == 1) {$(".modal-wrapper").show(200);}
    else{
        $(".modal-wrapper").hide(200);
    }

}