<?php
session_start();
require_once("controller/dbcontroller.php");
$db_handle = new DBController();
if(!empty($_GET["action"])) {

    switch($_GET["action"]) {
        case "login":
            $user = $_POST["user_admin_name"];
            $pswd = $_POST["pass_word_admin"];
            $user_validation = $db_handle->runQuery("SELECT * FROM users_56734 WHERE username = '" . $user . "' AND password = '". $pswd ."' ");

            if(!empty($user_validation)) {
                
                $_SESSION["admin"] = True;
                header('location: admin.php');
                exit;

            }
            else {
                
                header('location: ad-login.php?toast=authfail');
                exit;
            }

            break;
        case "destroy":

            unset($_SESSION["admin"]);
            header('location: admin.php');
            exit;
            break;
    }
}

?>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Inmed Corporation</title>
        <meta name="description" content="">
        <meta name="author" content="John Paul Burato">
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <link rel="icon" type="image/png" href="images/icon.ico">
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,600,800&display=swap" rel="stylesheet">
        
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css">
        <link rel="stylesheet" type="text/css" href="styles/common.css">
        <link rel="stylesheet" type="text/css" href="styles/main.css">
        <link rel="stylesheet" type="text/css" href="styles/login.css">

        <script src="scripts/jquery.js"></script>
        <script src="scripts/main.js"></script>
    </head>
    <body>
        
        <div class="login-wrapper">
            <h1 class="login-head"> Admin Login</h1>
            
            <form action="ad-login.php?action=login" method="POST" class="login-form">
            <?php 
            if(!empty($_GET["toast"])) {
                switch ($_GET["toast"]) { 
                    case 'loggedout': ?>
            <div class="button login-input success">LogIn session destroyed</div>
                        <?php
                        break;
                    case 'authfail': ?>
            <div class="button login-input fatal">ooof, wrong email/password</div>
                        <?php
                        break;
                    case 'attempt': ?>
            <div class="button login-input fatal">Viewing this page requires your admin credentials</div>
                    <?php
                }
            }
            ?>
                <input type="text" name="user_admin_name" class="login-input bordered"/>
                <input type="password" name="pass_word_admin" class="login-input bordered"/>
                <input type="submit" value="login" class="button login-input"/>
            </form>
            <div class="m-spacer"></div>
            <div class="align-center">
                
                <p>© 2019, Inmed Corporation All Rights Reserved.</p>
                <p>Go to our <a href="index.php" class="button">site</a></p>
            </div>
        </div>

        

        

    </body>
</html>