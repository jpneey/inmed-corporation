<?php
session_start();
require_once("controller/dbcontroller.php");
$db_handle = new DBController();

if(!empty($_GET["action"])) {

    switch($_GET["action"]) {
        case "add":
            if(!empty($_POST["quantity"])) {
                $productById = $db_handle->runQuery("SELECT * FROM products WHERE code='" . $_GET["id"] . "' ");
                $itemArray = array($productById[0]["code"]=>array('name'=>$productById[0]["name"], 'id'=>$productById[0]["code"], 'quantity'=>$_POST["quantity"], 'image'=>$productById[0]["image"]));

                if(!empty($_SESSION["cart_item"])) {
                    if(in_array($productById[0]["code"], array_keys($_SESSION["cart_item"]))) {

                        foreach($_SESSION["cart_item"] as $k => $v) {

                            if($productById[0]["code"] == $k) {
                                if(empty($_SESSION["cart_item"][$k]["quantity"])) {
                                    $_SESSION["cart_item"][$k]["quantity"] = 0;
                                }
                                $_SESSION["cart_item"][$k]["quantity"] += $_POST["quantity"];
                            }
                        }
                    } else {
                        $_SESSION["cart_item"] = array_merge($_SESSION["cart_item"],$itemArray);
                    }
                } else {
                    $_SESSION["cart_item"] = $itemArray;
                }

                
            header('Location: cart.php');
            exit;
            }
            break;
        case "update":
            if(!empty($_POST["quantity"])) {
                $productById = $db_handle->runQuery("SELECT * FROM products WHERE code='" . $_GET["id"] . "' ");
                $itemArray = array($productById[0]["code"]=>array('name'=>$productById[0]["name"], 'id'=>$productById[0]["code"], 'quantity'=>$_POST["quantity"], 'image'=>$productById[0]["image"]));

                if(!empty($_SESSION["cart_item"])) {
                    if(in_array($productById[0]["code"], array_keys($_SESSION["cart_item"]))) {

                        foreach($_SESSION["cart_item"] as $k => $v) {

                            if($productById[0]["code"] == $k) {
                                if(empty($_SESSION["cart_item"][$k]["quantity"])) {
                                    $_SESSION["cart_item"][$k]["quantity"] = 0;
                                }
                                $_SESSION["cart_item"][$k]["quantity"] = $_POST["quantity"];
                            }
                        }
                    } else {
                        $_SESSION["cart_item"] = array_merge($_SESSION["cart_item"],$itemArray);
                    }
                } else {
                    $_SESSION["cart_item"] = $itemArray;
                }

                
            header('Location: cart.php');
            exit;
            }
            break;
        case "remove":
            if(!empty($_SESSION["cart_item"])) {
                foreach($_SESSION["cart_item"] as $k => $v) {
                        if($_GET["id"] == $k)
                            unset($_SESSION["cart_item"][$k]);				
                        if(empty($_SESSION["cart_item"]))
                            unset($_SESSION["cart_item"]);
                }
            }
            header('Location: cart.php');
            exit;
	        break;

        case "empty":
            unset($_SESSION["cart_item"]);
            header('Location: cart.php');
            exit;
	        break;	
    }
}
?>


<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Inmed Corporation</title>
        <meta name="description" content="">
        <meta name="author" content="John Paul Burato">
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <link rel="icon" type="image/png" href="images/icon.ico">
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,600,800&display=swap" rel="stylesheet">
        
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css">
        <link rel="stylesheet" type="text/css" href="styles/common.css">
        <link rel="stylesheet" type="text/css" href="styles/main.css?v=1.2">
        <link rel="stylesheet" type="text/css" href="styles/cart.css">
        
        
        <script src="scripts/jquery.js"></script>
        <script src="scripts/main.js"></script>
        <script src="scripts/cart.js?v=1.1"></script>
    </head>
    <body>
    <div class="navigation">
            <img src="images/logo.png"/>
            
            <div class="mob-menu">
                <i class="fas fa-bars" onclick="sideNav(3)"></i>
            </div>
            <div class="navigation-menu">
                <ul id="horizontal-list">
                    <li><a href="../../index.html">Home</a></li>
                    <li><a href="../faqs.html">About us</a></li>
                    <li><a href="../apply.html">Be a reseller</a></li>
                    <li><a href="../careers.html">Careers</a></li>
                    <li><a href="../warranty.html">Warranty</a></li>
                    <li><a href="index.php">Products</a></li>
                    <li><a href="#!" onclick="sideNav(1)">Categories</a></li>
                    <?php 
                        if(isset($_SESSION["cart_item"])) {
                            $total_quantity = 0;
                            foreach ($_SESSION["cart_item"] as $item) {
                                $total_quantity += $item["quantity"];
                            }
                    ?>
                    <li id="cart-menu"><a href="cart.php"><i class="fas fa-shopping-cart"></i><span class="tq filled"><?php echo $total_quantity; ?> Item(s) - for quote</span></a></li>
                    <?php 
                        } else { ?>
                        <li><a href="cart.php"><i class="fas fa-shopping-cart"></i><span class="tq empty">I'm empty :(</span></a></li>
                    <?php
                    }
                    ?>
                </ul>
            </div>
        </div>
        <div class="sidemob">
            <div class="sidenavmob">
                <a onclick="sideNav(4)"><i class="fas fa-times"></i></a>
                <a href="../../index.html">Home</a>
                <a href="../faqs.html">About us</a>
                <a href="../apply.html">Be a reseller</a>
                <a href="../careers.html">Careers</a>
                <a href="../warranty.html">Warranty</a>
                <a href="index.php">Products</a>
                <a href="#!" onclick="sideNav(1)">Categories</a>
                <?php 
                    if(isset($_SESSION["cart_item"])) {
                        $total_quantity = 0;
                        foreach ($_SESSION["cart_item"] as $item) {
                            $total_quantity += $item["quantity"];
                        }
                ?>
                <a id="cart-menu"><a href="cart.php"><i class="fas fa-shopping-cart"></i><span class="cart-count-menu"> <?php echo $total_quantity; ?> Item(s)</span></a></a>
                <?php 
                    } else { ?>
                    <a href="cart.php"><i class="fas fa-shopping-cart"></i><span class="tq empty">I'm empty :(</span></a>
                <?php
                }
                ?>
            </div>
        </div>
        <div class="sidenav">
            <div>
                <a onclick="sideNav(2)"><i class="fas fa-times"></i></a>
                <?php
                $product_category = $db_handle->runQuery("SELECT * FROM category ORDER BY id ASC");
                if (!empty($product_category)) { 
                    foreach($product_category as $key=>$value){
                ?>
                    <a href="viewcategory.php?category=<?php echo $product_category[$key]['category']?>"><?php echo $product_category[$key]['category']?></a>
                <?php 
                    }
                } else {

                }
                
                ?>
            </div>
        </div>
        
        <div class="home-banner"></div>
        <div class="page-wrapper">
        <div class="cart-ui-wrapper">
            <div class="cart-ui-container">
                
                <div class="cart-ui-head cart-form align-center">QUOTATION CART</div>
            </div>

            <?php 
            if(isset($_SESSION["cart_item"])) {
                $total_quantity = 0;
                foreach ($_SESSION["cart_item"] as $item) {
                        $total_quantity += $item["quantity"]; ?> 
            <div class="cart-ui-container">
                <div class="cart-action">
                    <div><a href="cart.php?action=remove&id=<?php echo $item["id"]; ?>"><i class="far fa-times-circle i-cred"></i></a></div>
                </div>
                <div class="cart-action">
                    <p>
                        <form action="cart.php?action=update&id=<?php echo $item["id"]; ?>" method="POST" id="cart-total-machine">
                            <input type="number" name="quantity" value="<?php echo $item["quantity"]; ?>" class="cart-input" onkeypress="return isNumberKey(event)" min="1"/>
                            
                            <!-- <input type="submit" value=""  class="cart-input fa-input" /> -->
                            <button type="submit" class="cart-input fa-input"> <i class="fas fa-sync"></i> </button>
                        </form>
                        <span id="cart-total-mobile"><?php echo $item["quantity"]; ?></span>
                    </p>
                </div>
                <div class="cart-name">
                    <img src="<?php echo $item["image"]; ?>" />
                    <p><?php echo $item["name"]; ?></p>
                </div>

            </div>

            
        <?php
                } ?>

            <div class="cart-ui-container" style="padding-bottom: 15px;">
                <div class="cart-action">
                    <div>
                        <a href="cart.php?action=empty"><i class="far fa-trash-alt i-cred"></i></a>
                    </div>
                </div>
                <div class="cart-action">
                    <p class="align-left">Total:</p>
                </div>
                <div class="cart-name">
                    <p><?php echo $total_quantity; ?></p>
                    <a href="#!" class="button" onclick="checkout()">checkout</a>
                </div>
            </div> 
                
            
            
            <div class="cart-form-wrapper i-bwhite">
                <!-- <div class="cart-ui-head cart-form align-center">CHECKOUT</div> -->
                <div class="cart-form-wrapper">
                    <form action="controller/mailer.php" method="POST" class="cart-form" id="checkout-form">
                        <input required type="text" name="name" placeholder="name *" />
                        <input required type="number" maxlength="11" name="contactnumber" placeholder="mobilenumber (11-Digit number) *" onkeypress="return isNumberKey(event)"/>
                        <input required type="email" name="email" placeholder="email *" id="email"/>
                        <input required type="email" name="emailconfirmation" placeholder="confirm email *" id="confemail" />
                        <input type="number" maxlength="7" name="telephone" placeholder="phonenumber (7-digit Number)" onkeypress="return isNumberKey(event)"/>
                        <input required type="text" name="companyname" placeholder="companyname" />
                        <?php 
                        if(isset($_SESSION["cart_item"])) {
                            $total_quantity = 0;
                            foreach ($_SESSION["cart_item"] as $item) {
                                    $total_quantity += $item["quantity"]; ?>
                            <input type="hidden" name="item[]" value="<?php echo $item["name"]; ?> - <?php echo $item["quantity"]; ?> x" placeholder="item" hidden/>

                        <?php
                            }
                        } 
                        ?>
                        <textarea required name="message" rows="5" style="resize: none;"> </textarea>
                        <input type="submit" value="request quotation" class="button" id="submit-cart">
                        <p id="warranty-form-errors"></p>
                    </form>
                </div>
            </div> 







            <?php
            } else {?>
            <div class="no-records">Your Cart is Empty</div>

            <?php
            }
        ?>
        </div>
        </div>
        <?php 
            require("footer.php");
        ?>

    </body>
</html>