<?php
session_start();
/* error_reporting(0); */

if (isset($_SESSION['admin'])) {
    require_once("controller/dbcontroller.php");
    $db_handle = new DBController();
    if(!empty($_GET["action"])) {
        switch ($_GET["action"]) {

            case "execute":

                $postName = $_POST["name"];
                $postDescription = $_POST["description"];
                $postCategory = $_POST["category"];
                $postBrand = $_POST["brand"];
                $postPcode = $_POST["pcode"];
                $id = $_GET["id"];


                $currentDir = getcwd();
                $uploadDirectory = "/images/";
            
                $errors = []; // Store all foreseen and unforseen errors here
            
                $fileExtensions = ['jpeg','jpg','png']; // Get all the file extensions
            
                $fileName = $_FILES['img_description']['name'];
                $fileSize = $_FILES['img_description']['size'];
                $fileTmpName  = $_FILES['img_description']['tmp_name'];
                $fileType = $_FILES['img_description']['type'];
                $fileExtension = strtolower(end(explode('.',$fileName)));

                $uploadPath = $currentDir . $uploadDirectory . basename($fileName); 
            
                $imgdesc = "images/" . basename($fileName);
        
        
                if (! in_array($fileExtension,$fileExtensions)) {
                    $errors[] = "This file extension is not allowed. Please upload a JPEG or PNG file";
                }
        
                if ($fileSize > 2000000) {
                    $errors[] = "This file is more than 2MB. Sorry, it has to be less than or equal to 2MB";
                }
        
                if (empty($errors)) {
                    $didUpload = move_uploaded_file($fileTmpName, $uploadPath);
        
                    if ($didUpload) {
                        $delete_image = $db_handle->runQuery("SELECT * FROM products WHERE id='" . $_GET["a34xcvdm23in56yu89yt"] . "' ");
                        if (!empty($delete_image)) { 
                            foreach($delete_image as $key=>$value){ 
                                unlink($delete_image[$key]["img_description"]);
                            }
                        }
                        
                        $add = $db_handle->runQuery("UPDATE products SET name = '$postName', description = '$postDescription', price = '0', category = '$postCategory', brand = '$postBrand', img_description = '$imgdesc', product_code = '$postPcode' WHERE id = '$id' ");

                        header("location: edit.php?a34xcvdm23in56yu89yt=" . $id . "&success=true");
                        exit;
                        
                    }
                }
                            
                $add = $db_handle->runQuery("UPDATE products SET name = '$postName', description = '$postDescription', price = '0', category = '$postCategory', brand = '$postBrand', product_code = '$postPcode' WHERE id = '$id' ");

                header("location: edit.php?a34xcvdm23in56yu89yt=" . $id . "&success=metaimageerror");
                exit;
                break;

            case "upload":
                $currentDir = getcwd();
                $uploadDirectory = "/images/";
            
                $errors = []; // Store all foreseen and unforseen errors here
            
                $fileExtensions = ['jpeg','jpg','png']; // Get all the file extensions
            
                $fileName = $_FILES['myfile']['name'];
                $fileSize = $_FILES['myfile']['size'];
                $fileTmpName  = $_FILES['myfile']['tmp_name'];
                $fileType = $_FILES['myfile']['type'];
                $fileExtension = strtolower(end(explode('.',$fileName)));
                $uploadPath = $currentDir . $uploadDirectory . basename($fileName); 
            
                $fileDb = "images/" . basename($fileName);
            
            
                    if (! in_array($fileExtension,$fileExtensions)) {
                        $errors[] = "This file extension is not allowed. Please upload a JPEG or PNG file";
                    }
            
                    if ($fileSize > 2000000) {
                        $errors[] = "This file is more than 2MB. Sorry, it has to be less than or equal to 2MB";
                    }
            
                    if (empty($errors)) {
                        
                        $didUpload = move_uploaded_file($fileTmpName, $uploadPath);
                        $id = $_GET["id"];

                        if ($didUpload) {

                            $delete_image = $db_handle->runQuery("SELECT * FROM products WHERE id='$id' ");
                            
                            if (!empty($delete_image)) { 
                                foreach($delete_image as $key=>$value){ 
                                    unlink($delete_image[$key]["image"]);
                                }
                            }

                            $add = $db_handle->runQuery("UPDATE products SET image = '$fileDb' WHERE id = '$id'");
                            header("location: edit.php?a34xcvdm23in56yu89yt=" . $id . "&success=true");
                            exit;
                        
                        }
                    }
                
                $id = $_GET["id"];
                header("location: edit.php?a34xcvdm23in56yu89yt=" . $id . "&success=imageerror");
                exit;
                break;
            
            case "addGallery":
                    
                $currentDir = getcwd();
                $uploadDirectory = "/images/";
                
                $id = $_GET["productId"];
                $i = $_GET["galleryId"];

                $rand   = substr(md5(uniqid(mt_rand(), true)), 0, 3);
                $get = $db_handle->runQuery("SELECT * FROM products WHERE id='$id' ");
                

                if (!empty($get)) { 
                    foreach($get as $key=>$value){ 
                        $name = $get[$key]["name"];
                    }
                }
                
                

                ${"gallery".$i."TmpName"}  = $_FILES['new_image_gallery'.$i]['tmp_name'];
                ${"gallery".$i} = explode(".", $_FILES['new_image_gallery'.$i]["name"]);
                ${"gallery".$i."rand"} = $name . $rand . 'gal'. $i . '.' . end(${"gallery".$i});
                ${"gallery".$i."path"} = $currentDir . $uploadDirectory . basename(${"gallery".$i."rand"});

                $move = move_uploaded_file(${"gallery".$i."TmpName"}, ${"gallery".$i."path"});

                $filepath = 'images/' . ${"gallery".$i."rand"};    
                $to = "image_gallery" . $i;
                
                $db_handle->runQuery("UPDATE products SET $to = '$filepath' WHERE id = '$id' ");

                header("location: edit.php?a34xcvdm23in56yu89yt=" . $id . "&success=trues");
                break;

            case "updategallery":
            
                $id = $_GET["id"];
                $i = $_GET["gallery"];

                $delete_image = $db_handle->runQuery("SELECT * FROM products WHERE id='$id' ");
                

                if (!empty($delete_image)) { 
                    foreach($delete_image as $key=>$value){ 
                        unlink($delete_image[$key]["image_gallery".$i]);
                    }
                }
                $to = "image_gallery" . $i;
                
                $add = $db_handle->runQuery("UPDATE products SET $to = 'images/gallery.src' WHERE id = '$id'");
                header("location: edit.php?a34xcvdm23in56yu89yt=" . $id . "&success=true");
                break;
        }
    }
?>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Inmed Corporation</title>
        <meta name="description" content="">
        <meta name="author" content="John Paul Burato">
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,600,800&display=swap" rel="stylesheet">
        
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css">
        <link rel="stylesheet" type="text/css" href="styles/common.css">
        <link rel="stylesheet" type="text/css" href="styles/main.css">
        <link rel="stylesheet" type="text/css" href="styles/cart.css">
        <script src="scripts/jquery.js"></script>
        <script src="scripts/main.js"></script>
    </head>
    <body>
        <div class="navigation" style="background: #2d2d2d;">
            <div class="navigation-menu">
                <ul id="horizontal-list">
                    <li><a href="admin.php" style="color: #ffffff; border-left: 5px solid #2d2d2d;">DashBoard</a></li>
                    <li><a href="admin.php?a34xcvdm23in56yu89on=logout" style="color: #ffffff; border-left: 5px solid #2d2d2d;"><i class="fas fa-user"></i><span class="tq filled">Log Out</span></a></li>
                </ul>
            </div>
        </div>
        <div class="home-banner">
        
        </div>   
        <?php 
            if($_GET["success"] == 'imageerror') { ?>
            
            <div class="no-records">Image Upload failed. Please make sure that the file format is .jpg .jpeg .png or the file size is lower than 2mb</div>
            <?php
            }
            if ($_GET["success"] == 'metaimageerror') { ?>
            <div class="no-records warning">Meta description updated except image description.</div>
            
            <?php
            }
        ?>
        
        <div class="product-view-container">
            <?php
                $product_array = $db_handle->runQuery("SELECT * FROM products WHERE id='" . $_GET["a34xcvdm23in56yu89yt"] . "' ");
                if (!empty($product_array)) { 
                    foreach($product_array as $key=>$value){
            ?>
            <form action="edit.php?action=upload&id=<?php echo $product_array[$key]["id"]; ?>" method="POST" enctype="multipart/form-data" id="nyes">
                <div class="product-view">
                    <img src="<?php echo $product_array[$key]["image"]; ?>" class="view-img-main"/>
                    <div class="main_img">
                            <input required type="file" name="myfile" id="fileToUpload">
                            <label for="fileToUpload" class="button">
                                <i class="fas fa-cloud-upload-alt"></i> Edit Image
                            </label>
                            <br>
                    </div>
                    

                    <div>
                        <input type="submit" value="Save new product image" class="button align-center" style="width: 100%; background-color: #ff7e00;"/>
                    </div>
            </form>
            <?php 
                for($i=1;$i<=5;$i++) {
                    if ($product_array[$key]["image_gallery".$i] != 'images/gallery.src') { ?>
                        <div class="box">
                            <img src="<?php echo $product_array[$key]["image_gallery".$i]; ?>" class="box-gallery gallery1"/>
                        </div>
                        <div class="box-admin-full">
                            <div class="centered">
                                <a href="edit.php?action=updategallery&id=<?php echo $product_array[$key]["id"]; ?>&gallery=<?php echo $i; ?>" class="button i-bred float-l">Delete</a>
                            </div>
                        </div>
                    <?php
                    }
                    else if ($product_array[$key]["image_gallery".$i] = 'images/gallery.src') { ?>

                        <div class="box">
                            <img src="" class="box-gallery" id="gallery_empty<?php echo $i;?>"/>
                        </div>
                        <div class="box-admin-full">
                            <div class="centered">
                            <form action="edit.php?action=addGallery&productId=<?php echo $product_array[$key]["id"];?>&galleryId=<?php echo $i; ?>" method="POST" enctype="multipart/form-data">

                                <input required type="file" name="new_image_gallery<?php echo $i; ?>" class="button empty_gallery<?php echo $i; ?>" accept="image/*" />
                                <input type="submit" class="button i-bgreen float-l" value="save" />

                            </form>
                            </div>
                        </div>
                        
                    <?php
                    }
                }   
            ?>
                </div>

                <form action="edit.php?action=execute&id=<?php echo $product_array[$key]["id"]; ?>" method="POST" enctype="multipart/form-data" id="nyes">
                    <div class="product-view">
                        <div class="view-text-container">
                           
                            <h1><input required type="text" name="name" value="<?php echo $product_array[$key]["name"]; ?>"></h1>
                            <p class="align-left"><input required name="description" type="text" value="<?php echo $product_array[$key]["description"]; ?>"></p>
                            
                            <p class="align-left"><input required name="pcode" type="text" value="<?php echo $product_array[$key]["product_code"]; ?>"></p>
                            <p class="align-left"><input required name="brand" type="text" value="<?php echo $product_array[$key]["brand"]; ?>"></p>
                            
                            <p class="align-left">Product Description:</p>
                            <input type="file" name="img_description" id="filedescription">
                            <img src="<?php echo $product_array[$key]["img_description"]; ?>" class="" id="descimages"/>
                            
                            <p class="align-left">
                                <select required name="category" type="text" placeholder="category">
                                    <option value="<?php echo $product_array[$key]["category"]; ?>" selected><?php echo $product_array[$key]["category"]; ?></option>
                                    <?php 
                                    $category = $db_handle->runQuery("SELECT * FROM category ORDER BY id ASC");
                                    if (!empty($category)) { 
                                        foreach($category as $key=>$value){
                                    ?>
                                        <option value="<?php echo $category[$key]["category"] ?>"><?php echo $category[$key]["category"] ?></option>
                                    <?php 
                                        }
                                    }
                                    ?>
                                </select>
                            </p>
                        
                            <input type="submit" value="Update Product meta" class="button align-center"/>
                            <script src="scripts/edit.js"></script>
                        </div>
                    </div>
                    
                </form> 
        </div>
        
        <?php
                }
            }
        ?>
        
    </body>
</html>

<?php 
    }
    else {

        header('location: ad-login.php?toast=attempt');
        exit;
    }
?>