<?php
session_start();
error_reporting(0);

if (isset($_SESSION['admin'])) {
    require_once("controller/dbcontroller.php");
    $db_handle = new DBController();
    
?>


<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Inmed Corporation</title>
        <meta name="description" content="">
        <meta name="author" content="John Paul Burato">
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <link rel="icon" type="image/png" href="images/icon.ico">
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,600,800&display=swap" rel="stylesheet">
        
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css">
        <link rel="stylesheet" type="text/css" href="styles/common.css">
        <link rel="stylesheet" type="text/css" href="styles/main.css">
        <link rel="stylesheet" type="text/css" href="styles/admin.css">
        <link rel="stylesheet" type="text/css" href="styles/cart.css">
        
        
        <script src="scripts/jquery.js"></script>
        <script src="scripts/main.js"></script>
    </head>
    <body>
        <div class="navigation" style="background: #2d2d2d;">
            <div class="navigation-menu">
                <ul id="horizontal-list">
                    <li><a href="admin.php" style="color: #ffffff; border-left: 5px solid #2d2d2d;">DashBoard</a></li>
                    <li><a href="admin.php?a34xcvdm23in56yu89on=logout" style="color: #ffffff; border-left: 5px solid #2d2d2d;"><i class="fas fa-user"></i><span class="tq filled">Log Out</span></a></li>
                </ul>
            </div>
        </div>
        <div class="home-banner">
        
        </div>
        
        
        
        <div class="product-container">
            <div class="align-center">
                <p class="admin-panel-head">Product Lists</p>
                <div class="index-list-wrapper">
                    <div class="index-list-col align-center"><p>CODE</p></div>
                    <div class="index-list-col align-center"><p>CUSTOMER</p></div>
                    <div class="index-list-col align-center"><p>EMAIL</p></div>
                    <div class="index-list-col-m align-center"><p>DATE</p></div>
                </div>


            <div>
            <div class="align-center">
            <?php
            $product_array = $db_handle->runQuery("SELECT * FROM transactions ORDER BY id DESC");
            if (!empty($product_array)) { 
                foreach($product_array as $key=>$value){
            ?>
                <div class="index-list-wrapper">
                    <div class="index-list-col align-center"><p><?php echo $product_array[$key]["transaction_code"]; ?></p></div>
                    <div class="index-list-col align-center"><p><?php echo $product_array[$key]["customer_name"]; ?></p></div>
                    <div class="index-list-col align-center"><p><?php echo $product_array[$key]["email"]; ?></p></div>
                    <div class="index-list-col align-center"><p><?php echo $product_array[$key]["date"]; ?></p></div>
                    <div class="index-list-col align-center"><p><?php echo $product_array[$key]["items"]; ?></p></div>
                </div>
                
            <?php
                }
            }
            ?>
            
            </div>
        </div>
        
        

        

    </body>
</html>

<?php 
    }
    else {

        header('location: ad-login.php?toast=attempt');
        exit;
    }
?>