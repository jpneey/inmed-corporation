<?php
session_start();
error_reporting(0);

if (isset($_SESSION['admin'])) {
    require_once("controller/dbcontroller.php");
    $db_handle = new DBController();

    if(!empty($_GET["a34xcvdm23in56yu89on"])) {
        switch ($_GET["a34xcvdm23in56yu89on"]) {
            case "edit":
            
                $id = $_GET["a34xcvdm23in56yu89yt"];
                header("Location: edit.php?a34xcvdm23in56yu89yt=" . $id . "&success=null" );
                exit;
                break;

            case "delete":
                $id = $_GET["a34xcvdm23in56yu89yt"];
                $delete_image = $db_handle->runQuery("SELECT * FROM products WHERE id='$id' ");
                if (!empty($delete_image)) { 
                    foreach($delete_image as $key=>$value){ 
                        unlink($delete_image[$key]["image"]);
                        unlink($delete_image[$key]["img_description"]);
                        unlink($delete_image[$key]["image_gallery1"]);
                        unlink($delete_image[$key]["image_gallery2"]);
                        unlink($delete_image[$key]["image_gallery3"]);
                        unlink($delete_image[$key]["image_gallery4"]);
                        unlink($delete_image[$key]["image_gallery5"]);
                    }
                }
                $product_array = $db_handle->runQuery("DELETE FROM products WHERE id='$id' ");
                header("Location: admin.php");
                exit;
                break;
                
            case "categorydelete":
                $id = $_GET["categoryId"];
                $product_array = $db_handle->runQuery("DELETE FROM category WHERE id='$id' ");
                header("Location: admin.php");
                exit;
                break;
            
            case "add":
                header("Location: add.php?success=null");
                exit;
                break;
            case "logout":
                unset($_SESSION["admin"]);
                header('location: ad-login.php?toast=loggedout');
                exit;
                break;
        }
    }

?>


<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Inmed Corporation</title>
        <meta name="description" content="">
        <meta name="author" content="John Paul Burato">
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <link rel="icon" type="image/png" href="images/icon.ico">
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,600,800&display=swap" rel="stylesheet">

        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css">
        <link rel="stylesheet" type="text/css" href="styles/common.css">
        <link rel="stylesheet" type="text/css" href="styles/main.css">
        <link rel="stylesheet" type="text/css" href="styles/admin.css">
        <link rel="stylesheet" type="text/css" href="styles/cart.css">
        
        
        <script src="scripts/jquery.js"></script>
        <script src="scripts/main.js"></script>
        <script src="scripts/product.js"></script>
    </head>
    <body>
        <div class="navigation" style="background: #2d2d2d;">
            <!-- <img src="images/logo.png"/> -->
            <div class="navigation-menu">
                <ul id="horizontal-list">
                    <li><a href="admin.php" style="color: #ffffff; border-left: 5px solid #2d2d2d;">DashBoard</a></li>
                    <li><a href="add.php?success=null   " style="color: #ffffff; border-left: 5px solid #2d2d2d;">Add Products</a></li>
                    <li><a href="category.php" style="color: #ffffff; border-left: 5px solid #2d2d2d;">Add Categories</a></li>
                    
                    <li><a href="admin.php?a34xcvdm23in56yu89on=logout" style="color: #ffffff; border-left: 5px solid #2d2d2d;"><i class="fas fa-user"></i><span class="tq filled">Log Out</span></a></li>
                    
                </ul>
            </div>
        </div>
        <div class="home-banner">
        
        </div>
        
        <div class="product-container">
            <div class="admin-panel-wrapper">
                <div class="admin-panel align-center"> 
                    <p class="admin-panel-head">Admin Quick Menu</p>
                    
                    <div class="admin-panel-wrapper">   
                        <div class="admin-panel align-center no-border-panel">
                            <a href="category.php" class="admin-panel-link">Add Categories</a>
                        </div>  
                        <div class="admin-panel align-center no-border-panel">
                            <a href="add.php?success=null" class="admin-panel-link">Add Product</a>
                        </div>
                        <div class="admin-panel align-center no-border-panel">
                            <a href="transactions.php" class="admin-panel-link">View Transactions</a>
                        </div>
                        <div class="admin-panel align-center no-border-panel">
                            <a href="#!" class="admin-panel-link" onclick="productViewer(1)">View Products</a>
                        </div>
                    </div>
                </div>
                <div class="admin-panel align-center"> 
                    <?php $product_array = $db_handle->runQuery("SELECT COUNT(*) AS total FROM transactions"); ?>
                        <p class="admin-panel-head">total transactions proccessed</p> 
                        <p class=""><?php echo $product_array[0]["total"]; ?> </p> 
                </div>
                <div class="admin-panel align-center"> 
                    <?php $product_array = $db_handle->runQuery("SELECT COUNT(*) AS total FROM products"); ?>
                        <p class="admin-panel-head">total products in stock</p> 
                        <p class=""><?php echo $product_array[0]["total"]; ?> </p>    
                </div>
                <div class="admin-panel align-center"> 
                    <p class="admin-panel-head">Latest Transaction</p> 
                    <?php
                    $product_array = $db_handle->runQuery("SELECT * FROM transactions ORDER BY id DESC LIMIT 1");
                    if (!empty($product_array)) { 
                        foreach($product_array as $key=>$value){?>    
                        <p class="">Order no.  : <b> <?php echo $product_array[$key]["transaction_code"]; ?> </b> </p>
                        <p class="">Client name: <b> <?php echo $product_array[$key]["customer_name"]; ?> </b> </p>  
                        <p class="">Date : <b> <?php echo $product_array[$key]["date"]; ?> </b> </p>  

                    <?php
                        }
                    }
                    ?>
                </div>

                <div class="admin-panel align-center"> 
                    <p class="admin-panel-head">Latest Product</p> 
                    <?php
                    $product_array = $db_handle->runQuery("SELECT * FROM products ORDER BY id DESC LIMIT 1");
                    if (!empty($product_array)) { 
                        foreach($product_array as $key=>$value){?>    
                        <p class="">Product Name.  : <b> <?php echo $product_array[$key]["name"]; ?> </b> </p>
                        <p class="">Category: <b> <?php echo $product_array[$key]["category"]; ?> </b> </p>

                    <?php
                        }
                    }
                    ?>
                </div>
                
            </div>
            
        </div>
        
        <div class="product-container" id="products-listing">
            <div class="align-center">
                <p class="admin-panel-head">Product Lists</p>
                <div class="index-list-wrapper">
                    <div class="index-list-col align-center"><p>ID</p></div>
                    <div class="index-list-col align-center"><p>Name</p></div>
                    <div class="index-list-col align-center"><p>Category</p></div>
                    <div class="index-list-col-m align-center"><p>Action</p></div>
                </div>


            <div>
            <div class="align-center">
            <?php
            $product_array = $db_handle->runQuery("SELECT * FROM products ORDER BY id ASC");
            if (!empty($product_array)) { 
                foreach($product_array as $key=>$value){
            ?>
                <div class="index-list-wrapper">
                    <div class="index-list-col align-center"><p><?php echo $product_array[$key]["code"]; ?></p></div>
                    <div class="index-list-col align-center"><p><?php echo $product_array[$key]["name"]; ?></p></div>
                    <div class="index-list-col align-center"><p><?php echo $product_array[$key]["category"]; ?></p></div>
                    <div class="index-list-col align-center"><p><a href="admin.php?a34xcvdm23in56yu89on=edit&a34xcvdm23in56yu89yt=<?php echo $product_array[$key]["id"]; ?>">update </a></p></div>
                    <div class="index-list-col align-center"><p><a href="javascript:void(0);" id="<?php echo $product_array[$key]["id"];?>" onclick="alert(this)">delete </a></p></div>
                </div>
            <?php
                }
            }
            ?>
            
            </div>
        </div>
        <script>
            function alert(e) {

                let id = $(e).attr('id');
                if (window.confirm('Delete Product ?')){
                    console.log(id); 
                    window.location.href = ("admin.php?a34xcvdm23in56yu89on=delete&a34xcvdm23in56yu89yt="+id);
                }
                else{
                    return false;
                }


            };
        
        
        </script>
        
        

        

    </body>
</html>

<?php 
    }
    else {

        header('location: ad-login.php?toast=attempt');
        exit;
    }
?>