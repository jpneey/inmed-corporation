<?php
session_start();
require_once("controller/dbcontroller.php");
$db_handle = new DBController();
        
?>

<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Inmed Corporation</title>
        <meta name="description" content="">
        <meta name="author" content="John Paul Burato">
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <link rel="icon" type="image/png" href="images/icon.ico">
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,600,800&display=swap" rel="stylesheet">
        
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css">
        <link rel="stylesheet" type="text/css" href="styles/common.css">
        <link rel="stylesheet" type="text/css" href="styles/main.css">
        <link rel="stylesheet" type="text/css" href="styles/cart.css">

        <script src="scripts/jquery.js"></script>
        <script src="scripts/main.js"></script>
    </head>
    <body>
        
        <?php 
        ?>
        <div class="navigation">
            <img src="images/logo.png"/>
            
            <div class="mob-menu">
                <i class="fas fa-bars" onclick="sideNav(3)"></i>
            </div>
            <div class="navigation-menu">
                <ul id="horizontal-list">
                    <li><a href="../../index.html">Home</a></li>
                    <li><a href="index.php">Products</a></li>
                    <li><a href="#!" onclick="sideNav(1)">Categories</a></li>
                    <?php 
                        if(isset($_SESSION["cart_item"])) {
                            $total_quantity = 0;
                            foreach ($_SESSION["cart_item"] as $item) {
                                $total_quantity += $item["quantity"];
                            }
                    ?>
                    <li id="cart-menu"><a href="cart.php"><i class="fas fa-shopping-cart"></i><span class="tq filled"><?php echo $total_quantity; ?> Item(s) - for quote</span></a></li>
                    <?php 
                        } else { ?>
                        <li><a href="cart.php"><i class="fas fa-shopping-cart"></i><span class="tq empty">I'm empty :(</span></a></li>
                    <?php
                    }
                    ?>
                </ul>
            </div>
        </div>
        <div class="sidemob">
            <div class="sidenavmob">
                <a onclick="sideNav(4)"><i class="fas fa-times"></i></a>
                <a href="../../index.html">Home</a>
                <a href="index.php">Products</a>
                <a href="#!" onclick="sideNav(1)">Categories</a>
                <?php 
                    if(isset($_SESSION["cart_item"])) {
                        $total_quantity = 0;
                        foreach ($_SESSION["cart_item"] as $item) {
                            $total_quantity += $item["quantity"];
                        }
                ?>
                <a id="cart-menu"><a href="cart.php"><i class="fas fa-shopping-cart"></i><span class="cart-count-menu"> <?php echo $total_quantity; ?> Item(s)</span></a></a>
                <?php 
                    } else { ?>
                    <a href="cart.php"><i class="fas fa-shopping-cart"></i><span class="tq empty">I'm empty :(</span></a>
                <?php
                }
                ?>
            </div>
        </div>
        <div class="sidenav">
            <div>
                <a onclick="sideNav(2)"><i class="fas fa-times"></i></a>
                <?php
                $product_category = $db_handle->runQuery("SELECT * FROM category ORDER BY id ASC");
                if (!empty($product_category)) { 
                    foreach($product_category as $key=>$value){
                ?>
                    <a href="viewcategory.php?category=<?php echo $product_category[$key]['category']?>"><?php echo $product_category[$key]['category']?></a>
                <?php 
                    }
                } else {

                }
                
                ?>
            </div>
        </div>
        <div class="home-banner"></div>
        <div class="page-wrapper">
                
        <div class="product-container mob-cnt">
            <div class="title-wrapper">
                <h1 class="titles i-cblack">Thank you !</h1>
                <p>Your request for quotation was received</p>
            </div>
            <div class="cart-ui-wrapper">
                <div class="cart-ui-container">
                    <div class="thankyou cart-form align-center i-bblack">QUOTATION SLIP</div>
                </div>
            <?php
            $code = $_GET["code"];
            $array = $db_handle->runQuery("SELECT * FROM transactions WHERE transaction_code = '". $code ."' ");
            if (!empty($array)) { 
                foreach($array as $key=>$value){
            ?>
                <div class="thankyou cart-form align-left i-ty"><p>ORDER #: <?php echo $array[$key]["transaction_code"] ?></p></div>
                <div class="thankyou cart-form align-left i-ty"><p>CUSTOMER NAME: <?php echo $array[$key]["customer_name"] ?></p></div>
                <div class="thankyou cart-form align-left i-ty"><p>CUSTOMER EMAIL: <?php echo $array[$key]["email"] ?></p></div>
                <div class="thankyou cart-form align-left i-ty"><p>CONTACT NUMBER: <?php echo $array[$key]["mobile"] ?></p></div>
                
                <div class="cart-ui-container">
                    <div class="thankyou cart-form align-center i-bblack">ORDER DETAILS</div>
                </div>
            <?php
                    if(isset($_SESSION["thankyou"])) {
                        foreach ($_SESSION["thankyou"] as $item) { ?>
                            <div class="thankyou cart-form align-left i-ty">
                                
                                <p><?php echo $item["quantity"]; ?>x, <?php echo $item["name"]; ?></p>
                            </div>
                    <?php
                        }
                    }
                }
            }
            ?>
                <div class="cart-ui-container">
                    <div class="thankyou cart-form align-center i-borange">© 2019, Inmed Corporation All Rights Reserved</div>
                </div>
            </div>
        </div>

        </div>
        <div class="footer i-cwhite align-center">
            <p>© 2019, Inmed Corporation All Rights Reserved.</p>
        </div>

    </body>
</html>