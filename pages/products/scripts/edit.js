$("#fileToUpload").change(function () {
    filePreview(this);
});
$("#filedescription").change(function () {
    fileDesPreview(this);
});

$(".empty_gallery1").change(function () {
    gallery1(this, 1);
});
$(".empty_gallery2").change(function () {
    gallery1(this, 2);
});
$(".empty_gallery3").change(function () {
    gallery1(this, 3);
});
$(".empty_gallery4").change(function () {
    gallery1(this, 4);
});
$(".empty_gallery5").change(function () {
    gallery1(this, 5);
});

function filePreview(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#nyes + img').remove();
            $('.product-view > img').attr("src", "" + e.target.result + "");
        }
        reader.readAsDataURL(input.files[0]);
    }
}

function fileDesPreview(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#descimages').attr("src", "" + e.target.result + "");
        }
        reader.readAsDataURL(input.files[0]);
    }
}
function gallery1(input, e) {
    let c = e;
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        if(c === 1) {
            reader.onload = function (e) {
                $('#gallery_empty1').attr("src", "" + e.target.result + "");
            }
        }
        if(c === 2) {
            reader.onload = function (e) {
                $('#gallery_empty2').attr("src", "" + e.target.result + "");
            }
        }
        if(c === 3) {
            reader.onload = function (e) {
                $('#gallery_empty3').attr("src", "" + e.target.result + "");
            }
        }
        if(c === 4) {
            reader.onload = function (e) {
                $('#gallery_empty4').attr("src", "" + e.target.result + "");
            }
        }
        if(c === 5) {
            reader.onload = function (e) {
                $('#gallery_empty5').attr("src", "" + e.target.result + "");
            }
        }
        reader.readAsDataURL(input.files[0]);
    }
}