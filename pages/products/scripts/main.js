$(document).ready(function(){
    $('img[src="images"], img[src=""], img[src="images/error.src"]').attr({
        'src' : " images/product-img-placeholder.jpg",    
    });
    $('img[src="images/gallery.src"] ').parent().css({
        'display' : 'none'
    });

    $('.sidenav').hide();
    $('.sidemob').hide();
    $('#checkout-form').hide();
    $('.view-img-main').on('click', function(){
        console.log('clicked');
    });
});
$(window).scroll(function() {
    if ($(this).scrollTop() > 10){  
        $('.bged').css({
            'height' : '15%',
            'transition' : '0.5s'
        });
    }
    else{
        $('.bged').css({
            'height' : '35%',
            'transition' : '0.5s'
        });
    }
});
function checkout() {
    $('#checkout-form').show(500);
}

function sideNav(e) {
    if(e == 1) {
        $('.sidenav').show(200);
    }
    else if(e == 2) {
        $('.sidenav').hide(200);
    }
    else if(e == 3) {
        $('.sidemob').show(200);
    } 
    else if(e == 4) {
        $('.sidemob').hide(200);
    }


}

function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}

