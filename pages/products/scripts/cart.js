
function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}
$(document).ready( function(){ 
    $("#checkout-form").submit(function(event) {
        
        var email = document.getElementById("email").value
        var confemail = document.getElementById("confemail").value
    

        if (email != confemail) {
            event.preventDefault();
            $("#warranty-form-errors").text("Email and Email confirmation does not match. Kindly recheck your form inputs before submitting again.");
        }

        else {
            $(this).find(':input[type=submit]').prop('disabled', true);
        }
    });
})
function confirmEmail() {
    var email = document.getElementById("email").value
    var confemail = document.getElementById("confemail").value
    if(email != confemail) {
        $("#warranty-form-errors").text("Email and Email confirmation does not match. Kindly recheck your form inputs before submitting again.");
        $("#submit-cart").hide().slow(200);
    } else {
        $("#warranty-form-errors").text("");
        $("#submit-cart").show().slow(200);

    }
}

