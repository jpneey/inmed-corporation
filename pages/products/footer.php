<?php ?>

<html lang="en">
    <head>
        
    
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <link rel="stylesheet" type="text/css" href="styles/footer.css">
        <script src="../../scripts/main.js?v=1.1"></script>
    </head>
    <body>
    <div class="rows i-bblack i-footer" style="padding-bottom: 0;">
            <div class="global-container">
                <div class="global-three-col i-cwhite">
                    <div>
                        <div>
                            <div>
                                <h2>Visit Us</h2>
                            </div>
                            
                            <div class="flex margin-top">
                                <i class="fas fa-map-marked-alt"></i>
                                <p class="margin-auto">
                                    Inmed Corporation<br>
                                    5 Calle Industria, Bagumbayan,<br>
                                    Quezon City 1110 Philippines
                                </p>
                            </div>
                            <hr class="hr">
                        </div>
                        <div>
                            <div>
                                <h2>We Love Mails</h2>
                            </div>
                            
                            <div class="flex margin-top">
                                <i class="fas fa-envelope"></i>
                                <p class="margin-auto">
                                    To Buy Products from us:
                                    <a href="mailto:sales@inmed.com.ph?subject=Web%20Sales%20Inquiry" target="_top">sales@inmed.com.ph</a><br><br>
                                    To offer your Products and Services:
                                    <a href="mailto:purchasing@inmed.com.ph?subject=Web%20Purchasing%20Inquiry" target="_top">purchasing@inmed.com.ph</a>
                                </p>
                            </div>
                        </div>
                    </div>

                    <div>
                        <div>
                            <div>
                                <h2>Call Us</h2>
                            </div>
                            
                            <div class="flex margin-top">
                                <i class="fas fa-phone"></i>
                                <p class="margin-auto">
                                    +63.2.85711888<br>
                                    Globe Viber & WhatsApp<br>
                                    0917-81-INMED(46633)
                                </p>
                            </div>
                        </div>
                        <hr class="hr">
                        <div>
                            <div>
                                <h2>Quick links</h2>
                            </div>
                            <div class="flex margin-top">
                                <i class="fas fa-link"></i>
                                <div class="margin-auto">
                                    <a class="p" href="../../index.html">Top</a><br>
                                    <a class="p" href="../contact-us.html">Contact Us</a><br>
                                    <a class="p" href="http://inmed.com.ph/products">Products</a><br>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div>
                        <div>
                            <div>
                                <h2>NewsLetter</h2>
                            </div>
                            
                            <div class="flex margin-top">
                                <i class="fas fa-paper-plane"></i>
                                <p class="margin-auto">
                                    Subscribe to <br>
                                    our newsletter <br>
                                    to recieve exclusive offers!
                                </p>
                            </div>
                            <hr class="hr">

                            <div>
                                <div class="flex margin-top">
                                    <i class="fas fa-link"></i>
                                    <div class="margin-auto">
                                        <a class="p" href="http://inmed.com.ph/pages/contact-us.html#newsletter">Subscribe here</a><br>
                                        <a class="p" href="http://inmed.com.ph/pages/contact-us.html">Contact Us</a>
                                    </div>
                                </div>
                            </div>
                            <div class="spacer"><!-- none --></div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
        <div class="full-width align-center" style="margin: 0; background: black; padding: 5px;"><p style="margin: 0;padding: 5px; color: white; font-size: 10px;">© 2019. INMED CORPORATION ALL RIGHTS RESERVED.</p></div>
    </body>
</html> 
<?php ?>