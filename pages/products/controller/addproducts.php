<?php

session_start();

/* error_reporting(0); */

if (isset($_SESSION['admin'])) {
    require_once("dbcontroller.php");
    $db_handle = new DBController();

    if(!empty($_GET["action"])) {
        switch ($_GET["action"]) {

            case "execute":
                
                $currentDir = getcwd();
                $uploadDirectory = "/images/";
                $rand   = substr(md5(uniqid(mt_rand(), true)), 0, 3); 
            
                $errors = []; // Store all foreseen and unforseen errors here
            
                $fileExtensions = ['jpeg','jpg','png']; // Get all the file extensions
            
                $fileName = $_FILES['myfile']['name'];
                $fileSize = $_FILES['myfile']['size'];
                $fileTmpName  = $_FILES['myfile']['tmp_name'];
                $fileTmpName2  = $_FILES['img_description']['tmp_name'];
                $fileType = $_FILES['myfile']['type'];
                $fileExtension = strtolower(end(explode('.',$fileName)));

                $temp = explode(".", $_FILES["myfile"]["name"]);
                $newfilename = round(microtime(true)) . '.' . end($temp);

                $temp2 = explode(".", $_FILES["img_description"]["name"]);
                $newfilename2 = round(microtime(true)) . 'des.' . end($temp2);

            
                $nyes = "images/" . basename($fileName);
                $randomname = "images/" . basename($newfilename);
                $randomname2 = "images/" . basename($newfilename2);
            
                
                $uploadPath = $currentDir . $uploadDirectory . basename($newfilename) ; 
                $uploadPath2 = $currentDir . $uploadDirectory . basename($newfilename2) ; 

                //gallery
                $mcrt = round(microtime(true));
                for($i=0;$i <= 5; $i++) {    
                    ${"gallery".$i."TmpName"}  = $_FILES['gallery'.$i]['tmp_name'];
                    ${"gallery".$i} = explode(".", $_FILES['gallery'.$i]["name"]);
                    ${"gallery".$i."rand"} = $mcrt . $rand . 'gal'. $i . '.' . end(${"gallery".$i});
                    ${"gallery".$i."path"} = $currentDir . $uploadDirectory . basename(${"gallery".$i."rand"});
                }


                    if (! in_array($fileExtension,$fileExtensions)) {
                        $errors[] = "This file extension is not allowed. Please upload a JPEG or PNG file";
                    }
            
                    if ($fileSize > 2000000) {
                        $errors[] = "This file is more than 2MB. Sorry, it has to be less than or equal to 2MB";
                    }
            
                    if (empty($errors)) {
                        $didUpload = move_uploaded_file($fileTmpName, $uploadPath);
                        $didUpload2 = move_uploaded_file($fileTmpName2, $uploadPath2);
                        
                        for($i=0;$i <= 5; $i++) {  
                            
                            ${"gallery".$i} = move_uploaded_file(${"gallery".$i."TmpName"}, ${"gallery".$i."path"});
                        
                        }
            
                        if ($didUpload && $didUpload2) {
                            
                            $nme = $_POST["name"];
                            $stripped = substr($nme, 0, 1);
                            $rand   = $stripped . substr(md5(uniqid(mt_rand(), true)), 0, 3);     
                            $uniqrand   = uniqid();
                            $code = $rand . $uniqrand . $stripped;
                            
                            $filepath = $randomname; //image_main
                            $filepath2 = $randomname2; //img_des
                            
                            for($i=0;$i <= 5; $i++) { 
                                if(${"gallery".$i}) {
                                    ${"gallery".$i} = 'images/' . ${"gallery".$i."rand"};
                                } else {
                                    ${"gallery".$i} = 'images/gallery.src';
                                }
                            } 
                            
                            $add = $db_handle->runQuery("INSERT INTO products(name, description, price, category, image, code, brand, img_description, product_code, image_gallery1, image_gallery2, image_gallery3, image_gallery4, image_gallery5) VALUES('". $_POST["name"] ."', '". $_POST["description"] ."', '0', '". $_POST["category"] ."', '". $filepath ."', '". $code ."', '". $_POST["brand"] ."', '". $filepath2 ."', '". $_POST["pcode"] ."', '". $gallery1 ."', '". $gallery2 ."', '". $gallery3 ."', '". $gallery4 ."', '". $gallery5 ."')");
                            
                            /* header("location: ../add.php?prime=" . $filepath . "&success=true");
                            exit; */
                        }
                    }
                /* header("location: ../add.php?prime=" . $filepath . "&success=imageerror");
                exit; */
                break;
            case "upload":
                /* header("location: ../add.php?prime=" . $nyes . "&success=imageerror");
                break; */
        }
    }
} else {
    header("location: ../index.php");
}
?>