<?php
    use PHPMailer\PHPMailer\PHPMailer;
    use PHPMailer\PHPMailer\Exception;


    require 'dbcontroller.php';
    /* Exception class. */
    require 'PHPMailer\src\Exception.php';

    /* The main PHPMailer class. */
    require 'PHPMailer\src\PHPMailer.php';

    /* SMTP class, needed if you want to use SMTP. */
    require 'PHPMailer\src\SMTP.php';

    
    /* $db_handle = new DBController();
    $mail = new PHPMailer();
    $mail->IsSMTP();
    $mail->SMTPDebug = 1;
    $mail->SMTPAuth = true;
    $mail->SMTPSecure = 'ssl';
    $mail->Host = "smtp.gmail.com";
    $mail->Port = 465;
    $mail->IsHTML(true);
    $mail->Username = "inmedcph@gmail.com";
    $mail->Password = "password123!"; */
    
    /* 
    $mail->SetFrom("inmedcph@gmail.com");
    $mail->Subject = "Web Quotation Request";  */
   

    $db_handle = new DBController();
    $mail = new PHPMailer(); 
    $mail->IsSMTP(); 
    $mail->SMTPDebug = 0; 
    $mail->SMTPAuth = true; 
    $mail->Host = "smtp.inmed.com.ph";
    $mail->Username = "inquiry@inmed.com.ph";
    $mail->Password = "Inmed123!";
    $mail->IsHTML(true);


    /* passed form values */
    $itms = $_POST['item'];

    $name       = $_POST['name'];
    $mobile     = $_POST['contactnumber'];
    $email      = $_POST['email'];
    $phone      = $_POST['telephone'];
    $company    = $_POST['companyname'];
    $note       = $_POST["message"];
    $items      = '';

    $fullName = $_POST['name'];
    
    $mail->SetFrom("inquiry@inmed.com.ph", "Quotation Request - " . $fullName);
    $mail->Subject = "Quotation Request - " . $fullName;

    
    foreach( $itms as $itm ) {
        $items .= $itm;
        $items .= '<br>';
    }
    $htmlMessage = '
    <!DOCTYPE html>
    <html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Inmed - Warranty Registration</title>
        <style>
            html,
            body,
            table,
            tbody,
            tr,
            td,
            div,
            p,
            ul,
            ol,
            li,
            h1,
            h2,
            h3,
            h4,
            h5,
            h6 {
                margin: 0;
                padding: 0;
            }
            body {
                margin: 0;
                padding: 0;
                font-size: 0;
                line-height: 0;
                -ms-text-size-adjust: 100%;
                -webkit-text-size-adjust: 100%;
            }
            table {
                border-spacing: 0;
                mso-table-lspace: 0pt;
                mso-table-rspace: 0pt;
            }
            table td {
                border-collapse: collapse;
            }
            .ExternalClass {
                width: 100%;
            }
            .ExternalClass,
            .ExternalClass p,
            .ExternalClass span,
            .ExternalClass font,
            .ExternalClass td,
            .ExternalClass div {
                line-height: 100%;
            }
            /* Outermost container in Outlook.com */
            .ReadMsgBody {
                width: 100%;
            }
            img {
                -ms-interpolation-mode: bicubic;
            }
            h1,
            h2,
            h3,
            h4,
            h5,
            h6 {
                font-family: Arial;
            }
            h1 {
                font-size: 28px;
                line-height: 32px;
                padding-top: 10px;
                padding-bottom: 24px;
            }
            h2 {
                font-size: 24px;
                line-height: 28px;
                padding-top: 10px;
                padding-bottom: 20px;
            }
            h3 {
                font-size: 20px;
                line-height: 24px;
                padding-top: 10px;
                padding-bottom: 16px;
            }
            p {
                font-size: 16px;
                line-height: 20px;
                font-family: Georgia, Arial, sans-serif;
            }
            </style>
            <style>
                
            .container600 {
                width: 600px;
                max-width: 100%;
            }
            @media all and (max-width: 599px) {
                .container600 {
                    width: 100% !important;
                }
            }
        </style>
    
        <!--[if gte mso 9]>
            <style>
                .ol {
                  width: 100%;
                }
            </style>
        <![endif]-->
    
    </head>
    <body style="background-color:#F4F4F4;">
        <center>
    
              <!--[if gte mso 9]><table width="600" cellpadding="0" cellspacing="0"><tr><td>
                        <![endif]-->
          <table class="container600" cellpadding="0" cellspacing="0" border="0" width="100%" style="width:calc(100%);max-width:calc(600px);margin: 0 auto;">
            <tr>
              <td width="100%" style="text-align: left;">
    
                <table width="100%" cellpadding="0" cellspacing="0" style="min-width:100%;">
                    <tr>
                        <td style="background-color:#FFFFFF;color:#000000;padding:30px;">
                            <img alt="Inmed Corporation" src="http://inmed.com.ph/assets/inmed%20logo.png" width="200" style="display: block;" />
                        </td>
                    </tr>
                </table>
                <table width="100%" cellpadding="0" cellspacing="0" style="min-width:100%;">
                    <tr>
                        <td style="background-color:#F8F7F0;color:#58585A;padding:30px;">
    
                            <h1 style="text-align:center;"> Quotation Request </h1>
                            <p> </p>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding:20px;background-color:#F8F7F0;">
    
                                <table width="100%" cellpadding="0" cellspacing="0" style="min-width:100%;">
                                    <thead>
                                    <tr>
                                        <th scope="col" style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;line-height:30px"></th>
                                        <th scope="col" style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;line-height:30px"></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    
                                    <tr>
                                        <td valign="top" style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;">Name:</td>
                                        <td valign="top" style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;">' . $name . '</td>
                                    </tr>
                                    <tr>
                                        <td valign="top" style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;">Email:</td>
                                        <td valign="top" style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;">' . $email . '</td>
                                    </tr>
                                    <tr>
                                        <td valign="top" style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;">Company:</td>
                                        <td valign="top" style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;">' . $company . '</td>
                                    </tr>
                                    <tr>
                                        <td valign="top" style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;">Phone:</td>
                                        <td valign="top" style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;">' . $phone . '</td>
                                    </tr>
                                    <tr>
                                        <td valign="top" style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;">Mobile Number:</td>
                                        <td valign="top" style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;">' . $mobile . '</td>
                                    </tr>
                                    
                                    <tr>
                                        <td valign="top" style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;">Order Notes:</td>
                                        <td valign="top" style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;">' . $note . '</td>
                                    </tr>
                                    <tr>
                                        <td valign="top" style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;">Order:</td>
                                        <td valign="top" style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;">' . $items . '</td>
                                    </tr>
                                    
                                    </tbody>
                                </table>
    
                        </td>
                    </tr>
                </table>
                <table width="100%" cellpadding="0" cellspacing="0" style="min-width:100%;">
                    <tr>
                        <td width="100%" style="min-width:100%;background-color:#58585A;color:#58585A;padding:30px;">
                            <p style="font-size:16px;line-height:20px;font-family:Georgia,Arial,sans-serif;text-align:center;">-</p>
                        </td>
                    </tr>
                </table>
                </td>
            </tr>
        </table>
    
      <!--[if gte mso 9]></td></tr></table>
                        <![endif]-->
        </center>
    </body>
    </html>
    ';
                
    $mail->Body = $htmlMessage;



    $stripped = "qt_";
    $rand   = $stripped . substr(md5(uniqid(mt_rand(), true)), 0, 3);     
    $uniqrand   = uniqid();
    $code = $rand . $uniqrand;   
    $postName = $_POST["name"];
    $postEmail = $_POST["email"];
    $postContact = $_POST["contactnumber"];
    $date = date("Y/m/d");

    
    session_start();
    $itemCommaString = $_SESSION["cart_item"];
    $orders = "";
    foreach($itemCommaString as $key=>$value) {
        
        $a =  $itemCommaString[$key]["name"];
        $b = $itemCommaString[$key]["quantity"] . " x";
        $orders .= $a . " - " . $b .", ";
    }


    $db_handle->runQuery("INSERT INTO transactions(transaction_code, customer_name, email, mobile, date, items) VALUES('$code', '$postName', '$postEmail',  '$postContact', '$date', '$orders')");
    $mail->AddAddress("sales@inmed.com.ph");
    /* $mail->AddAddress("burato348@gmail.com"); */

    if(!$mail->Send()) {
        header("Location: ../thankyou.php?status=error&code=" . $code);
    } else {
        unset($_SESSION["thankyou"]);
        $_SESSION["thankyou"] = $_SESSION["cart_item"];
        unset($_SESSION["cart_item"]);
        header("Location: ../thankyou.php?status=done&code=" . $code);
    }
?>