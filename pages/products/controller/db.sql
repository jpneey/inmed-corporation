CREATE TABLE `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(255) NOT NULL,
   PRIMARY KEY (id)
);

INSERT INTO `category` (`category`) VALUES
('Medical Supplies'),
('Laboratory Supplies'),
('Mobility Aids'),
('Hospital Equipment'),
('Diagnostic Devices'),
('Home Care'),
('uncategorized');

-------------------------------------------------------

CREATE TABLE `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `description` text NOT NULL,
  `price` double NOT NULL,
  `category` varchar(128) NOT NULL,
  `image` varchar(100) NOT NULL,
  `code` varchar(15) NOT NULL,
  `brand` varchar(20) NOT NULL,
  `img_description` varchar(100) NOT NULL,
  `product_code` varchar(100) NOT NULL,
  `image_gallery1` varchar(255) NOT NULL,
  `image_gallery2` varchar(255) NOT NULL,
  `image_gallery3` varchar(255) NOT NULL,
  `image_gallery4` varchar(255) NOT NULL,
  `image_gallery5` varchar(255) NOT NULL,
  PRIMARY KEY (id)
);
-------------------------------------------------------

CREATE TABLE `transactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `transaction_code` varchar(255) NOT NULL,
  `customer_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `mobile` varchar(255) NOT NULL,
  `date` varchar(255) NOT NULL,
  `items` varchar(255) NOT NULL,
  PRIMARY KEY (id)
);

-------------------------------------------------------


CREATE TABLE `users_56734` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `usermail` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  PRIMARY KEY (id)
);

INSERT INTO `users_56734` (`username`, `usermail`, `password`) VALUES
('554311', '554321', '554321'),
('jpneey', 'jpadmin@mail.me', 'jipsin'),
('pmc-admin', 'pmc-admin@mail.me', '753951456852');

-------------------------------------------------------

/* 
http://inmed.com.ph/index.html
testmailer585@gmail.com
deathwish

%trf4yvX!34d#l6dWNy*

https://inmed-inquirer.000webhostapp.com/ */




/* Username: 7WxQggk2sK

Database name: 7WxQggk2sK

Password: cUEiXv6rJW

Server: remotemysql.com

Port: 3306 */