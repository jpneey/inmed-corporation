-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 29, 2019 at 10:48 AM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.1.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `inmed_quotation_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `category` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `category`) VALUES
(1, 'Medical Supplies'),
(2, 'Laboratory Supplies'),
(3, 'Mobility Aids'),
(4, 'Hospital Equipment'),
(5, 'Diagnostic Devices'),
(6, 'Home Care'),
(7, 'uncategorized');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `description` text NOT NULL,
  `price` double NOT NULL,
  `category` varchar(128) NOT NULL,
  `image` varchar(100) NOT NULL,
  `code` varchar(15) NOT NULL,
  `brand` varchar(20) NOT NULL,
  `img_description` varchar(100) NOT NULL,
  `product_code` varchar(100) NOT NULL,
  `image_gallery1` varchar(255) NOT NULL,
  `image_gallery2` varchar(255) NOT NULL,
  `image_gallery3` varchar(255) NOT NULL,
  `image_gallery4` varchar(255) NOT NULL,
  `image_gallery5` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `description`, `price`, `category`, `image`, `code`, `brand`, `img_description`, `product_code`, `image_gallery1`, `image_gallery2`, `image_gallery3`, `image_gallery4`, `image_gallery5`) VALUES
(13, 'Air Cushion Ring', 'Red, Nice, Comfy', 0, 'Home Care', 'images/1567041285.png', 'Ab1f5d672704de6', 'default', 'images/1567041285des.jpg', '3513', 'images/1567041285b01gal1.png', 'images/Air Cushion Ringa92gal2.png', 'images/Air Cushion Ringd02gal3.png', 'images/gallery.src', 'images/gallery.src'),
(20, 'TMS Anti-Bedsore Pump and Mattress, Lattice Style', 'Foamy, Nice Bed', 0, 'Hospital Equipment', 'images/1567065712.png', 'Tab35d67866fbd9', 'default', 'images/1567065712des.jpg', '3513', 'images/1567065712bc6gal1.png', 'images/1567065712bc6gal2.png', 'images/gallery.src', 'images/gallery.src', 'images/gallery.src');

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE `transactions` (
  `id` int(11) NOT NULL,
  `transaction_code` varchar(255) NOT NULL,
  `customer_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `mobile` varchar(255) NOT NULL,
  `date` varchar(255) NOT NULL,
  `items` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transactions`
--

INSERT INTO `transactions` (`id`, `transaction_code`, `customer_name`, `email`, `mobile`, `date`, `items`) VALUES
(1, 'qt_b505d67357e509f0', 'pmcinquirer', 'burato349@gmail.com', '0987', '', ''),
(12, 'qt_61e5d67794184ae6', 'JP', 'burato348@gmail.com', '09296209056', '2019/08/29', 'JP - 1 x, Air Cushion Ring - 1 x, '),
(13, 'qt_0455d677fd581845', 'Test', 'test@mail.me', '12321312321', '2019/08/29', 'Air Cushion Ring - 1 x, '),
(14, 'qt_e385d6790ca42021', 'JP', 'me@mail.me', '09684561234', '2019/08/29', 'TMS Anti-Bedsore Pump and Mattress, Lattice Style - 1 x, ');

-- --------------------------------------------------------

--
-- Table structure for table `users_56734`
--

CREATE TABLE `users_56734` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `usermail` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users_56734`
--

INSERT INTO `users_56734` (`id`, `username`, `usermail`, `password`) VALUES
(1, '554311', '554321', '554321'),
(2, 'jpneey', 'jpadmin@mail.me', 'jipsin');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_56734`
--
ALTER TABLE `users_56734`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `transactions`
--
ALTER TABLE `transactions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `users_56734`
--
ALTER TABLE `users_56734`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
