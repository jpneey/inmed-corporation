<?php
    use PHPMailer\PHPMailer\PHPMailer;
    use PHPMailer\PHPMailer\Exception;

    /* Exception class. */
    require 'PHPMailer\src\Exception.php';

    /* The main PHPMailer class. */
    require 'PHPMailer\src\PHPMailer.php';

    /* SMTP class, needed if you want to use SMTP. */
    require 'PHPMailer\src\SMTP.php';

    
    $mail = new PHPMailer(); 
    $mail->IsSMTP(); 
    $mail->SMTPDebug = 0; 
    $mail->SMTPAuth = true; 
    $mail->Host = "smtp.inmed.com.ph";
    $mail->Username = "inquiry@inmed.com.ph";
    $mail->Password = "Inmed123!";
    $mail->IsHTML(true);

   


    /* $mail = new PHPMailer();
    $mail->IsSMTP();
    $mail->SMTPDebug = 0;
    $mail->SMTPAuth = true;
    $mail->SMTPSecure = 'ssl';
    $mail->Host = "smtp.gmail.com";
    $mail->Port = 465;
    $mail->IsHTML(true);
    $mail->Username = "pmcmailchimp@gmail.com";
    $mail->Password = "1_pmcmailchimp@gmail.com"; */
    

    $fname      =    $_POST['firstname'];
    $lname      =    $_POST['lastname'];

    $name       = '' . $fname . ' ' . $lname;

    $mail->SetFrom("inquiry@inmed.com.ph", "" . $name );
    $mail->Subject = "Warranty Registration - " . $name ;

    $email      =    $_POST['email'];
    
    $saddress   =    $_POST['streetaddress'];
    $city       =    $_POST['city'];
    $state      =    $_POST['state'];
    
    $zip        =    $_POST['zipcode'];
    $country    =    $_POST['country'];

    $address    =   '' . $saddress . '<br>&#9;' . $city . '<br>&#9;' . $state . '<br>&#9;' . $zip . '<br>&#9;' . $country . '<br>';

    $fax      =    $_POST['faxnumber'];
    $mobile      =    $_POST['mobilenumber'];

    $phone      =    $_POST['phonenumber'];
    
    $item      =    $_POST['productitem'];

    $pfrom      =    $_POST['purchasefrom'];

    $pprice      =    $_POST['purchaseprice'];
    $pdate      =    $_POST['purchasedate'];
    $snumber      =    $_POST['serialnumber'];
    $age      =    $_POST['userage'];

    $q1     = $_POST['q1'];
    if ($q1 == "other") {
        $q1 = $_POST["q9"];
    }

    $q2array = $_POST['q2'];
    $q2 = implode(", ", $q2array);
    
    $q3ans = $_POST['q3'];
    $q3com = $_POST['q3com'];

    $q3 = $q3ans . ' <br>' . $q3com;
    
    $q4ans = $_POST['q4'];
    $q4com = $_POST['q4com'];

    $q4 = $q4ans . ' <br>' . $q4com;

    $q5 =$_POST["q5"];

    if (isset($_SERVER['HTTP_CLIENT_IP']) ? $_SERVER['HTTP_CLIENT_IP'] : isset($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR']) {
        $ip = isset($_SERVER['HTTP_CLIENT_IP']) ? $_SERVER['HTTP_CLIENT_IP'] : isset($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR'];
    }
    else {
        $ip = 'IP Not set';
    }

    $htmlMessage = '
            <!DOCTYPE html>
            <html>
            <head>
                <meta charset="utf-8">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <title>Inmed - Warranty Registration</title>
                <style>
                    html,
                    body,
                    table,
                    tbody,
                    tr,
                    td,
                    div,
                    p,
                    ul,
                    ol,
                    li,
                    h1,
                    h2,
                    h3,
                    h4,
                    h5,
                    h6 {
                        margin: 0;
                        padding: 0;
                    }
                    body {
                        margin: 0;
                        padding: 0;
                        font-size: 0;
                        line-height: 0;
                        -ms-text-size-adjust: 100%;
                        -webkit-text-size-adjust: 100%;
                    }
                    table {
                        border-spacing: 0;
                        mso-table-lspace: 0pt;
                        mso-table-rspace: 0pt;
                    }
                    table td {
                        border-collapse: collapse;
                    }
                    .ExternalClass {
                        width: 100%;
                    }
                    .ExternalClass,
                    .ExternalClass p,
                    .ExternalClass span,
                    .ExternalClass font,
                    .ExternalClass td,
                    .ExternalClass div {
                        line-height: 100%;
                    }
                    /* Outermost container in Outlook.com */
                    .ReadMsgBody {
                        width: 100%;
                    }
                    img {
                        -ms-interpolation-mode: bicubic;
                    }
                    h1,
                    h2,
                    h3,
                    h4,
                    h5,
                    h6 {
                        font-family: Arial;
                    }
                    h1 {
                        font-size: 28px;
                        line-height: 32px;
                        padding-top: 10px;
                        padding-bottom: 24px;
                    }
                    h2 {
                        font-size: 24px;
                        line-height: 28px;
                        padding-top: 10px;
                        padding-bottom: 20px;
                    }
                    h3 {
                        font-size: 20px;
                        line-height: 24px;
                        padding-top: 10px;
                        padding-bottom: 16px;
                    }
                    p {
                        font-size: 16px;
                        line-height: 20px;
                        font-family: Georgia, Arial, sans-serif;
                    }
                    </style>
                    <style>
                        
                    .container600 {
                        width: 600px;
                        max-width: 100%;
                    }
                    @media all and (max-width: 599px) {
                        .container600 {
                            width: 100% !important;
                        }
                    }
                </style>

                <!--[if gte mso 9]>
                    <style>
                        .ol {
                        width: 100%;
                        }
                    </style>
                <![endif]-->

            </head>
            <body style="background-color:#F4F4F4;">
                <center>

                    <!--[if gte mso 9]><table width="600" cellpadding="0" cellspacing="0"><tr><td>
                                <![endif]-->
                <table class="container600" cellpadding="0" cellspacing="0" border="0" width="100%" style="width:calc(100%);max-width:calc(600px);margin: 0 auto;">
                    <tr>
                    <td width="100%" style="text-align: left;">

                        <table width="100%" cellpadding="0" cellspacing="0" style="min-width:100%;">
                            <tr>
                                <td style="background-color:#FFFFFF;color:#000000;padding:30px;">
                                    <img alt="Inmed Corporation" src="http://inmed.com.ph/assets/inmed%20logo.png" width="200" style="display: block;" />
                                </td>
                            </tr>
                        </table>
                        <table width="100%" cellpadding="0" cellspacing="0" style="min-width:100%;">
                            <tr>
                                <td style="background-color:#F8F7F0;color:#58585A;padding:30px;">

                                    <h1 style="text-align:center;">Inmed Warranty Registration</h1>
                                    <p> </p>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:20px;background-color:#F8F7F0;">

                                        <table width="100%" cellpadding="0" cellspacing="0" style="min-width:100%;">
                                            <thead>
                                            <tr>
                                                <th scope="col" style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;line-height:30px;"></th>
                                                <th scope="col" style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;line-height:30px;"></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            
                                            <tr>
                                                <td valign="top" style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;">Name: </td>
                                                <td valign="top" style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;">'. $name .'</td>
                                            </tr>
                                            <tr>
                                                <td valign="top" style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;">Email: </td>
                                                <td valign="top" style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;">'. $email .'</td>
                                            </tr>
                                            <tr>
                                                <td valign="top" style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;">Address :</td>
                                                <td valign="top" style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;">'. $address . '</td>
                                            </tr>
                                            <tr>
                                                <td valign="top" style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;">Phone:</td>
                                                <td valign="top" style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;">'. $fax . '</td>
                                            </tr>
                                            <tr>
                                                <td valign="top" style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;">Mobile:</td>
                                                <td valign="top" style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;">'. $mobile . '</td>
                                            </tr>
                                            <tr>
                                                <td valign="top" style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;">Item :</td>
                                                <td valign="top" style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;">' . $item . '</td>
                                            </tr>
                                            <tr>
                                                <td valign="top" style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;">Purchased from:</td>
                                                <td valign="top" style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;">' . $pfrom . '</td>
                                            </tr>
                                            <tr>
                                                <td valign="top" style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;">Purchase price:</td>
                                                <td valign="top" style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;">' . $pprice . '</td>
                                            </tr>
                                            <tr>
                                                <td valign="top" style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;">Date of purchase:</td>
                                                <td valign="top" style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;">' . $pdate . '</td>
                                            </tr>
                                            
                                            <tr>
                                                <td valign="top" style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;">Serial Number:</td>
                                                <td valign="top" style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;">' . $snumber .'</td>
                                            </tr>
                                            <tr>
                                                <td valign="top" style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;">End User Bracket age:</td>
                                                <td valign="top" style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;">' . $age . '</td>
                                            </tr>
                                            <tr>
                                                <td valign="top" style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;">How did you first become aware of Inmed Products ?</td>
                                                <td valign="top" style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;">' . $q1 . '</td>
                                            </tr>
                                            
                                            <tr>
                                                <td valign="top" style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;">What Factor(s) Influenced you to purchase Inmed Products? </td>
                                                <td valign="top" style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;">' . $q2 . '</td>
                                            </tr>
                                            <tr>
                                                <td valign="top" style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;">How would you rate the packaging of Inmed Products ? </td>
                                                <td valign="top" style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;">' . $q3 . '</td>
                                            </tr>
                                            <tr>
                                                <td valign="top" style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;">Overall how satisfied are you with the products and services that we provide ? </td>
                                                <td valign="top" style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;">' . $q4 . '</td>
                                            </tr>
                                            <tr>
                                                <td valign="top" style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;">Comments and suggestions:</td>
                                                <td valign="top" style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;">' . $q5 . '</td>
                                            </tr>
                                            </tbody>
                                        </table>

                                </td>
                            </tr>
                        </table>
                        <table width="100%" cellpadding="0" cellspacing="0" style="min-width:100%;">
                            <tr>
                                <td width="100%" style="min-width:100%;background-color:#58585A;color:#ffffff;padding:30px;">
                                    <p style="font-size:12px;line-height:20px;font-family: Arial,sans-serif;text-align:center;">Sender\'s IP: '. $ip .'</p>
                                </td>
                            </tr>
                        </table>
                        </td>
                    </tr>
                </table>

            <!--[if gte mso 9]></td></tr></table>
                                <![endif]-->
                </center>
            </body>
            </html>
            ';
                
    $mail->Body = $htmlMessage;
    
    $mail->AddAddress("sales@inmed.com.ph");
    /* $mail->AddAddress("burato348@gmail.com"); */
   
    if(!$mail->Send()) {
        echo "Mailer Error: " . $mail->ErrorInfo;
    } else {
        header("Location: http://inmed.com.ph/pages/thankyou.html");
        exit;
    }
?>